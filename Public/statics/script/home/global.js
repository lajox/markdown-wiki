//全局执行函数
define(function(require, exports, module) {
    var $ = require('jquery');
    //require('jquery/plugins/jquery.md5');
    require('jquery/plugins/jquery.cookie.js');

    //var Handlebars = require('handlebars');
    var Handlebars = require('handlebars-helper');

    $(function() {

        $(document).on('click', 'a.jsUrlLink', function(e) {
            e.preventDefault();
            var target = $(this).attr('target');
            target = target || '_self';
            if(target=='self') {
                target = '_self';
            }
            var url = $(this).attr('data-url');
            if(target=='self' || target == '_self') {
                location.href = url;
            }
            else {
                window.open(url, target);
            }
        });

    });

    //控制导航头nav选中
    window.nav={
        selected:function(id){
            $(".jsNavItem[data-id='"+id+"']").addClass('selected').siblings().removeClass('selected');
        }
    };

    /**
     * 定义全局函数
     */

    Function.prototype.delay = function(timeout) {
        var __method = this,
            args = Array.prototype.slice.call(arguments, 1);
        timeout = timeout * 1000;
        return window.setTimeout(function() {
            return __method.apply(__method, args);
        }, timeout);
    };

    String.prototype.param = function() {
        var o = {};
        var a = this.split('&');
        for(var i in a) {
            var b = a[i].split('=');
            o[b[0]] = b[1];
        }
        return o;
    };

    //判断函数是否存在
    exports.function_exists = function(f) {
        var isFunction = false;
        try{
            var isString = Object.prototype.toString.call(f) === "[object String]";
            if(isString) {
                isFunction = typeof(eval(f))=="function";
            }
            else {
                isFunction = typeof(f)=="function";
            }
        }catch(e){}
        return isFunction;
    };

    //读取cookie
    exports.getCookie = function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length, c.length);
            }
        };
        return null;
    };

    //设置cookie
    exports.setCookie = function(name, value, days) {
        var argc = exports.setCookie.arguments.length;
        var argv = exports.setCookie.arguments;
        var secure = (argc > 5) ? argv[5] : false;
        var expire = new Date();
        if(days==null || days==0) days=1;
        expire.setTime(expire.getTime() + 3600000*24*days);
        document.cookie = name + "=" + escape(value) + ("; path=/") + ((secure == true) ? "; secure" : "") + ";expires="+expire.toGMTString();
    };

});

