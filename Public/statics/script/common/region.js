define("common/region.js", [ "common/dropdown.js" ], function(e, t, n) {
    "use strict";
    var $ = e("jquery");
    var i = e("common/dropdown.js");
    var s = function(e) {
        this.opt = e, this.container = $(e.container), this.container.find(".dropdown_menu").length > 0 && this.container.html(""), c.call(this);
    };
    var o;
    var u = "/Home/Public/getareas?id=";
    var a = {
        province: "省份",
        city: "城市"
    };
    var f = function(e) {
        return e === undefined && (e = ""), e += "", e && e.replace(/(\u00a0|&nbsp;)/g, " ").replace(/&quot;/ig, '"').replace(/&#39;/ig, "'");
    };
    var l = function(e, t) {
        $.get(u + (e || -1), function(d){
            t(d);
        });
    };
    var c = function() {
        var e = this;
        e.opt.list ? $.each([ "province", "city" ], function(t, n) {
            e.opt.list[n] = e.opt.list[n] || [];
        }) : e.opt.list = {
            province: [],
            city: []
        };
        e.opt.display = e.opt.display || {
                province: !0,
                city: !0
            };
        e.selectors = {};
        $.each([ "province", "city" ], function(t, n) {
            var r = "js_" + n + (Math.random() * 1e4 | 0);
            $('<div id="' + r + '" style="display:none"></div>').appendTo(e.container);
            e.selectors[n] = "#" + r;
        });
        h.call(this, "0", "province");
    };
    var h = function(e, t) {
        var n = this;
        o = e;
        n.status = "loading";
        l(e, function(r) {
            if (o != e) return;
            n.status = "loaded";
            var i = [], s = n.opt.list[t];
            $.each(r.data, function(e, t) {
                var n = f($.trim(t.name)).replace(/"/g, "&quot;");
                if("中国" == n) {
                    i.unshift({
                        name: n,
                        value: t.id
                    });
                }
                else {
                    i.push({
                        name: n,
                        value: t.id
                    });
                }
            });
            i = s.concat(i);
            p.call(n, t, i);
            var u = n.opt.data;
            u && u[t] && n[t].selected(u[t]);
            (d.call(n, t) || n.opt.display[t] == 0) && n[t].container.hide();
            (d.call(n, t) || t == "city") && n.opt.onComplete && n.opt.onComplete.call(n);
        });
    };
    var p = function(e, t) {
        var n = this;
        n[e] = new i({
            container: n.selectors[e],
            label: a[e],
            data: t,
            callback: function(t, r) {
                switch (e) {
                    case "province":
                        h.call(n, t, "city");
                }
                switch (e) {
                    case "province":
                        n.city && n.city.container.hide();
                        n.city = null;
                }
                n.opt.onChange && n.opt.onChange(e, t, r);
            }
        });
        n[e].container.show();
    };
    var d = function(e) {
        return this.status != "loading" && (this[e] == null || this[e].opt.data.length == 0);
    };
    var v = {
        get: function(e) {
            return d.call(this, e) ? null : this[e].name;
        },
        getV: function(e) {
            return d.call(this, e) ? null : this[e].value;
        },
        getAll: function() {
            return {
                province: this.get("province"),
                city: this.get("city"),
                province_id: this.getV("province"),
                city_id: this.getV("city"),
            };
        },
        set: function(e) {
            h.call(this, "0", "province");
            cosole.log(this);
        }
    };
    $.extend(s.prototype, v);
    n.exports = s;
});