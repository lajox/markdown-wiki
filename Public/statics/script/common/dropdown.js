define(
    "common/dropdown.js",
    [
        "jquery",
        "common/dropdown.js",
        "common/tpl/dropdown.css",
        "common/tpl/dropdown.html.js",
        "handlebars",
        "handlebars-helper",
    ],
    function(e, t, n) {
        "use strict";
        var $ = e("jquery");

        //var Handlebars = e('handlebars');
        var Handlebars = e('handlebars-helper');

        function r(e) {
            function t() {
                $(".jsDropdownList").hide(), $(".dropdown_menu").removeClass("open");
            }

            var n = this;
            n.container = $(e.container);
            n.container.addClass(s);
            e.render && typeof e.render && (e.renderHtml = "", $.each(e.data, function (t, n) {
                e.renderHtml += e.render(n);
            }));
            e = $.extend(!0, {}, o, e);
            e.disabled && n.container.addClass("disabled");
            n.opt = e;
            var myTemplate = Handlebars.compile(i);
            var str = myTemplate(e);
            n.container.html(str).find(".jsDropdownList").hide();
            n.bt = n.container.find(".jsDropdownBt");
            n.dropdown = n.container.find(".jsDropdownList");
            $.each(e.data, function (e, t) {
                t.value && $.isPlainObject(t.value) && $.data(n.dropdown.find(".jsDropdownItem")[e], "value", t.value);
            });
            if(typeof e.index != "undefined") {
                n.bt.find(".jsBtLabel").text(e.data[e.index].name || e.label);
                n.bt.find(".jsBtLabel").attr("data-name", e.data[e.index].name || e.label);
                n.bt.find(".jsBtLabel").attr("data-value", e.data[e.index].value || "");
                n.value = e.data[e.index].value;
            }
            n.bt.on("click", function () {
                return t(), e.disabled || (n.dropdown.show(), n.container.addClass("open")), !1;
            });
            $(document).on("click", t);
            n.dropdown.on("click", ".jsDropdownItem", function (t) {
                var r = $(this).data("value"),
                    i = $(this).data("name"),
                    s = $(this).data("index");
                if (!n.value || n.value && n.value != r) {
                    n.value = r;
                    n.name = i;
                    if(e.callback && typeof e.callback == "function") {
                        var k = e.callback(r, i, s) || i;
                        n.bt.find(".jsBtLabel").text(k);
                        n.bt.find(".jsBtLabel").attr("data-name", i);
                        n.bt.find(".jsBtLabel").attr("data-value", r);
                    }
                }
                n.dropdown.hide();
            });
        }

        e("common/tpl/dropdown.css");
        var i = e("common/tpl/dropdown.html.js"),
            s = "dropdown_menu",
            o = {
                label: "请选择",
                data: [],
                callback: $.noop,
                render: $.noop,
                delay: 500,
                disabled: !1
            };
        r.prototype = {
            selected: function (e) {
                var t = this;
                if (typeof e == "number") {
                    if (this.opt.data && this.opt.data[e]) {
                        var n = this.opt.data[e].name,
                            r = this.opt.data[e].value;
                        this.dropdown.find(".jsDropdownItem:eq(" + e + ")").trigger("click", r);
                        this.bt.find(".jsBtLabel").text(n);
                        this.bt.find(".jsBtLabel").attr("data-name", n);
                        this.bt.find(".jsBtLabel").attr("data-value", r);
                    }
                }
                else {
                    $.each(this.opt.data, function (i, s) {
                        if (e == s.value || e == s.name) {
                            t.dropdown.find(".jsDropdownItem:eq(" + i + ")").trigger("click", r);
                            t.bt.find(".jsBtLabel").text(n);
                            t.bt.find(".jsBtLabel").attr("data-name", n);
                            t.bt.find(".jsBtLabel").attr("data-value", t.dropdown.find(".jsDropdownItem:eq(" + i + ")").attr('data-value') || "");
                            return !1;
                        }
                    });
                }
                return this;
            },
            reset: function () {
                this.bt.find(".jsBtLabel").text(this.opt.label);
                this.bt.find(".jsBtLabel").attr("data-name", this.opt.label);
                this.bt.find(".jsBtLabel").attr("data-value", "");
                this.value = null;
                return this;
            },
            destroy: function () {
                return this.container.children().remove(), this.container.off(), this;
            },
            enable: function () {
                return this.opt.disabled = !1, this.container.removeClass("disabled"), this;
            },
            disable: function () {
                return this.opt.disabled = !0, this.container.addClass("disabled"), this;
            }
        };
        n.exports = r;
    });