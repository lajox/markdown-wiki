# Markdown-Wiki
> #### 相关资源
* <b>官方网站 : [永久网络](http://www.19www.com) </b> :point_left:

> #### 学习交流
* 框架交流群：![](https://raw.githubusercontent.com/JackJiang2011/MobileIMSDK/master/preview/more_screenshots/others/qq_group_icon_16-16.png) `41161311` :point_left:
* bug/建议发送至邮箱：`lajox@19www.com`
* 技术支持/合作/咨询请联系作者QQ：`517544292`

### 一、简介 ###

<b>Markdown-Wiki是一个简单易用的Markdown文档系统：</b>
 
* 只需要您写好的Markdown文档内容进行保存即可。
* 它摆脱了在线编辑器排版困难，无法实时预览的缺点，一切都交给Markdown来完成，一篇文档就是一个Markdown文件。
* 支持表格排版，代码高亮，数学公式等常用功能。 

### 二. 功能特点 ###

1. 使用Markdown
2. 支持表格排版
3. 支持代码高亮
4. 支持Latex数学公式
5. 伪静态页面访问

### 三. 系统优势 ###
 
1. 使用Markdown编写，摆脱后台编辑排版困难，无法实时预览的缺点  
2. 全站伪静态输出   
3. 完善的后台管理功能  

### 四. 环境要求 ###

PHP 5.3.0+

### 五. 安装步骤 ###

1. 下载markdown-wiki源代码  
2. 解压上传到你的PHP网站根目录  
3. 打开浏览器，访问网站首页  
4. 登陆后台，管理文档 

### 六. 问题及bug反馈 ###

如果在实际使用过程中对Markdown-Wiki有新的功能需求，或者在使用Markdown-Wiki的过程中发现了Bug，欢迎反馈给我。可以直接在Github上提交，也可以发邮件至`517544292@qq.com`与我取得联系，我将及时回复。如果你正在使用markdown-wiki，也可以告诉我，我将也会在这里列出使用者名单。如果你想和其他Markdown-Wiki使用者讨论交流，欢迎加入QQ群`41161311`。

### 七. 感谢 ###

Markdown-Wiki的成长需要喜欢Markdown，喜欢写博客的各位亲们支持！感谢你们使用Markdown-Wiki，感激你们对Markdown-Wiki的良好建议与Bug反馈。

QQ群：`41161311`  
作者邮箱：`lajox@19www.com`

### 八. 演示测试 ###

前台地址： [http://120.76.180.226:2081](http://120.76.180.226:2081)

后台地址： [http://120.76.180.226:2081/Admin](http://120.76.180.226:2081/Admin)

- 用户名： admin
- 密码： 123456


