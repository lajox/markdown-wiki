<?php
// 数据库配置
$config = [
	'DB_TYPE'               => 'mysql',
	'DB_HOST'               => '127.0.0.1',
	'DB_NAME'               => 'wiki',
	'DB_PREFIX'               => 'wk_',
	'DB_USER'               => 'root',
	'DB_PWD'                => 'root',
	'DB_PORT'               => '3306',
];

return $config;
