<?php
namespace Admin\Model;
class ConfigModel extends \Think\Model{

    //表名
    public $tableName = "config";
    public $groupTable = "config_group";
    public $roleTable = "role";

    //获得配置组
    public function getCfgGroup($isshow = 1)
    {
        return M($this->groupTable)->where("isshow=$isshow")->order("cgorder ASC, cgid asc")->select();
    }

    //获得配置组
    public function getCfg($isshow = 1)
    {
        $group = M($this->groupTable)->where("isshow=$isshow")->order("cgorder ASC, cgid asc")->select();
        foreach ($group as $id => $g) {
            $map['cgid'] = array('EQ', $g['cgid']);
            $config = $this->where($map)->order("order_list ASC, id asc")->select();
            if ($config) {
                foreach ($config as $i => $c) {
                    $func = '_' . $c['show_type']; //text radio select group textarea
                    $config[$i]['_html'] = $this->$func($c);
                }
            }
            $group[$id]['_config'] = $config;
        }
        return $group;
    }

    private function _text($config)
    {
        return "<input type='text' name='config[{$config['id']}][value]' value='{$config['value']}' id='{$config['name']}' class='took-w300'/>";
    }

    private function _radio($config)
    {
        $info = explode(',', $config['info']);
        $html = '';
        foreach ($info as $radio) {
            $data = explode('|', $radio); //[0]值如1  [1]描述如开启
            $checked = $data[0] == $config['value'] ? ' checked="checked" ' : '';
            $html .= "<label><input type='radio' name='config[{$config['id']}][value]' value='{$data[0]}' $checked/> {$data[1]}</label> ";
        }
        return $html;
    }

    private function _textarea($config)
    {
        return "<textarea class='took-w300 took-h100' name='config[{$config['id']}][value]'>{$config['value']}</textarea>";
    }

    //列表选项
    private function _select($config)
    {
        $info = explode(',', $config['info']);
        $html = "<select name='config[{$config['id']}][value]' class='took-w300'>";
        foreach ($info as $radio) {
            $data = explode('|', $radio); //[0]值如1  [1]描述如开启
            $selected = $data[0] == $config['value'] ? ' selected="selected" ' : '';
            $html .= "<option value='{$data[0]}' $selected> {$data[1]}</option> ";
        }
        $html .= "</select>";
        return $html;
    }

    //会员组
    private function _group($config)
    {
        $map['admin'] = array('EQ', 0);
        $map['rid'] = array('NEQ', 4); //不是游客
        $memberROle = M($this->roleTable)->where($map)->select();
        $html = "<select name='config[{$config['id']}][value]' class='took-w300'>";
        foreach ($memberROle as $id => $role) {
            $selected = $role['rid'] == $config['value'] ? ' selected="" ' : '';
            $html .= "<option value='{$role['rid']}' $selected>{$role['rname']}</option>";
        }
        $html .= "</select>";
        return $html;
    }

    /**
     * 添加配置
     * @return bool|int
     */
    public function addCfg()
    {
        $name = I('name', '', 'strtoupper');
        $map['name'] = array('EQ', $name);
        if (empty($name)) {
            $this->error = '变量名不能为空';
            return false;
        }
        if (M($this->tableName)->where($map)->find()) {
            $this->error = '变量名已经存在';
            return false;
        }
        if (!empty($_POST['info'])) {
            $_POST['info'] = str_replace(' ', '', $_POST['info']);//参数
            $_POST['info'] = \Common\Lib\Tool\String::toSemiangle($_POST['info']);//转拼音
        }
        //验证变量名
        if (M($this->tableName)->find(array('name' => $_POST['name']))) {
            $this->error = '变量名已经存在';
            return false;
        }
        if ($this->add()) {
            return $this->updateCache();
        }
        else {
            $this->error = '操作失败';
        }
    }

    /**
     * 删除配置
     * @return int
     */
    public function delCfg()
    {
        $id = I('id', 0, 'intval');
        if ($this->delete($id)) {
            return $this->updateCache();
        }
    }

    /**
     * 修改配置
     * @return bool|int
     */
    public function editCfg()
    {
        $configData = $_POST['config'];
        if (!is_array($configData)) {
            $this->error = '数据不能为空';
            return false;
        }
        $db = M($this->tableName);
        foreach ($configData as $name => $data) {
            $db->save($data);
        }
        return $this->updateCache();
    }

    /**
     * 更新配置文件
     * @return int
     */
    public function updateCache()
    {
        $configData = $this->order('order_list ASC')->select();
        $data = array();
        foreach ($configData as $c) {
            $name = strtoupper($c['name']);
            $data[$name] = $c['value'];
        }
        //写入配置文件
        $content = "<?php \nreturn " . var_export($data, true) . ";\n?>";
        return file_put_contents(CONF_PATH . "app.php", $content);
    }
}