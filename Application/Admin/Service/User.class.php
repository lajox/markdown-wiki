<?php
namespace Admin\Service;
class User {

    //存储用户uid的Key
    const userUidKey = 'sys_uid';
    //超级管理员角色id
    const administratorRoleId = 1;

    //当前登录会员详细信息
    private static $userInfo = array();

    /**
     * 返回一个实例
     */
    static public function getInstance() {
        static $handier = NULL;
        if (empty($handier)) {
            $handier = new static();
        }
        return $handier;
    }

    /**
     * 魔术方法
     * @param type $name
     * @return null
     */
    public function __get($name) {
        //从缓存中获取
        if (isset(self::$userInfo[$name])) {
            return self::$userInfo[$name];
        } else {
            $userInfo = $this->getInfo();
            if (!empty($userInfo)) {
                return $userInfo[$name];
            }
            return NULL;
        }
    }

    /**
     * 获取当前登录用户资料
     * @return array 
     */
    public function getInfo() {
        if (empty(self::$userInfo)) {
            self::$userInfo = $this->getUserInfo($this->isLogin());
        }
        return !empty(self::$userInfo) ? self::$userInfo : false;
    }

    /**
     * 检验用户是否已经登陆
     * @return boolean 失败返回false，成功返回当前登陆用户基本信息
     */
    public function isLogin() {
        $uid = \Common\Lib\Tool\Encry::decrypt(session(self::userUidKey));
        if (empty($uid)) {
            return false;
        }
        return (int) $uid;
    }

    //登录后台
    public function login($uname, $password) {
        if (empty($uname) || empty($password)) {
            return false;
        }
        //验证
        $userInfo = $this->getUserInfo($uname, $password);
        if (false == $userInfo) {
            $this->error = '用户名或者密码错误';
            //记录登录日志
            $this->record($uname, $password, 0);
            return false;
        } else {
            if($userInfo['admin']==0) {
                $this->error = '此账户不是管理员,没有权限';
                return false;
            }
        }
        $this->error = '';
        //记录登录日志
        $this->record($uname, $password, 1);
        //注册登录状态
        $this->registerLogin($userInfo);
        return true;
    }

    /**
     * 检查当前用户是否超级管理员
     * @return boolean
     */
    public function isAdministrator() {
        $userInfo = $this->getInfo();
        if (!empty($userInfo) && $userInfo['rid'] == self::administratorRoleId) {
            return true;
        }
        return false;
    }

    /**
     * 注销登录状态
     * @return boolean
     */
    public function logout() {
        //清空SESSION
        //session('[destroy]');
        session_unset();
        session_destroy();
        return true;
    }

    /**
     * 记录登陆日志
     * @param int $uid 登陆用户ID
     * @param string $password 密码
     * @param int $status
     */
    private function record($uid, $password, $status = 0) {
        //登录日志
        D('Admin/LoginLog')->addLog(array(
            "username" => $uid,
            "status" => $status,
            "info" => is_int($uid) ? '用户ID登录' : '用户名登录',
        ));
    }

    /**
     * 注册用户登录状态
     * @param array $user 用户信息
     */
    private function registerLogin(array $user) {
        //写入session
        session(self::userUidKey, \Common\Lib\Tool\Encry::encrypt((int) $user['uid']));
        //更新状态
        D('Admin/User')->loginState((int) $user['uid']);
    }

    /**
     * 获取用户信息
     * @param int $uid 用户ID
     * @return boolean|array
     */
    private function getUserInfo($uid, $password = NULL) {
        if (empty($uid)) {
            return false;
        }
        return D('Admin/User')->getUserInfo($uid, $password);
    }

}
