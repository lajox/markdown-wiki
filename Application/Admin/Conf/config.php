<?php
return array(
	//'配置项'=>'配置值'
    'SHOW_PAGE_TRACE' => false,
    'SHOW_RUN_TIME' => false,

    'URL_MODEL' => 0,
    //自定义模板的字符串替换
    'TMPL_PARSE_STRING'=>array(
        '__PUBLIC__'=>__ROOT__.'/Public',
    ),
    'TAGLIB_BEGIN'          => '<',
    'TAGLIB_END'            => '>',
    /* 模板引擎设置 */
    'TMPL_CONTENT_TYPE'     =>  'text/html', // 默认模板输出类型
    'TMPL_ACTION_ERROR'     =>  'Public/error', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  'Public/success', // 默认成功跳转对应的模板文件
    /* 分页设置 */
    'VAR_PAGE' => 'page', // 分页标识符
    'PAGE_SIZE' => 10, //每页显示数量
    //其他配置
    'AUTH_KEY'  => 'ADMIN_KEY',
);
