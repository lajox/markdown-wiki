<?php
namespace Admin\Controller;
class IndexController extends BaseController{
    public function _initialize() {
        parent::_initialize();
    }

    /**
     * 后台首页
     */
    public function index()
    {
        $this->display();
    }

}
