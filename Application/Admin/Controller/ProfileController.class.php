<?php
namespace Admin\Controller;
class ProfileController extends BaseController{

    private $_mod;

    public function _initialize() {
        parent::_initialize();
        $this->_mod = D('User');
    }

    /**
     * 个人资料
     */
    public function profile()
    {
        if (IS_POST) {
            $_POST['uid'] = $this->userInfo['uid'];
            if ($this->_mod->editUser($_POST)) {
                $this->success('修改成功', U('profile'));
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $user = $this->adminUser->getInfo();
            $this->assign('user', $user);
            $this->display();
        }
    }

    /**
     * 修改密码
     */
    public function password()
    {
        if (IS_POST) {
            $_POST['uid'] = $this->userInfo['uid'];
            $oldPass = I('post.old_password', '', 'trim'); //旧密码
            $newPass = I('post.new_password', '', 'trim'); //新密码
            $rePass = I('post.re_password', '', 'trim'); //确认新密码
            if (empty($oldPass)) {
                $this->error("请输入旧密码！"); exit;
            }
            if ($newPass != $rePass) {
                $this->error("两次密码不相同！"); exit;
            }
            if ($this->_mod->changePassword($this->userInfo['uid'], $newPass, $oldPass)) {
                //退出登陆
                $this->adminUser->logout();
                $this->success("密码已经更新，请从新登陆！", C('USER_AUTH_GATEWAY'));
            } else {
                $error = $this->_mod->getError();
                $this->error($error ? $error : "操作失败！");
            }
        } else {
            $user = $this->adminUser->getInfo();
            $this->assign('user', $user);
            $this->display();
        }
    }
}